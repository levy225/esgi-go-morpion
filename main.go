package main

import (
	"net/http"
    "fmt"
    "encoding/json"

	"github.com/thedevsaddam/renderer"
)

// constantes globales
const (
    tailleDamier   = 9
    symboleJoueur1 = "X"
    symboleJoueur2 = "O"
)

// Variables globales
var (
    tableauMorpion = [tailleDamier]string{ // création d'un damier sans aucune cases remplies
        "1", "2", "3",
        "4", "5", "6",
        "7", "8", "9"}
    joueur1 = true // c'est le joueur 1 qui commence en 1er
)


type tour struct {
    Damier [9]string `json:damier`
    Joueur1 bool `json:joueur1`
    Victoire int `json:victoire`
}

type Tours []tour


type Jeu struct {
	Case int `json:"case"`
}


var rnd *renderer.Render

func init() {
	opts := renderer.Options{
		ParseGlobPattern: "./tpl/*.html",
	}
	rnd = renderer.New(opts)
}

func getGrille(w http.ResponseWriter, r *http.Request) {

    if r.URL.Path == "/" {

        switch r.Method {
            case "GET":
               fmt.Println(tableauMorpion)
               data := Tours{
                  tour{tableauMorpion,joueur1, 0},
               }
               w.Header().Set("content-type", "application/json")
               json.NewEncoder(w).Encode(data)

            case "POST":
                w.Header().Set("content-type", "application/json")
                w.Write([]byte(`{"message": "hello %s"}`))
        }

    }
}

func handlerJouer(channelIn chan int) func(w http.ResponseWriter, r *http.Request) {


    return func (w http.ResponseWriter, r *http.Request) {

       switch r.Method {
            case "POST":

                w.Header().Set("content-type", "application/json")

                var res map[string]int
                json.NewDecoder(r.Body).Decode(&res)
                fmt.Println(res["case"])

                channelIn <- int(res["case"])

                test(channelIn)

                 data := Tours{
                      tour{tableauMorpion,joueur1,0},
                    }

                if gagner() { // Vérifier si le joueur a gagné
                    fmt.Println("vous avez gagné !")
                    data = Tours{
                      tour{tableauMorpion,joueur1,1},
                    }


                } else if partieNulle() { // Vérifier si match nul

                    fmt.Println("Partie nulle !")
                    data = Tours{
                      tour{tableauMorpion,joueur1,2},
                    }

                }
                joueur1 = !joueur1

                json.NewEncoder(w).Encode(data)
       }

    }
}

func handleGrille(w http.ResponseWriter, r *http.Request) {
    tableauMorpion = [tailleDamier]string{"1","2","3","4","5","6","7","8","9"}
	rnd.HTML(w, http.StatusOK, "index.html", nil)
}

func main() {
	channelIn := make(chan int)
	go test(channelIn)

	http.HandleFunc("/", getGrille)
	http.HandleFunc("/jouer", handlerJouer(channelIn))
	http.HandleFunc("/grille", handleGrille)
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	http.ListenAndServe(":8003", nil)
}


/**
* description de la fonction : Permet de lancer le jeu
*
* @return rien
*/
func jouer(numeroCase int) []tour {

    remplirCase(numeroCase)


    if gagner() { // Vérifier si le joueur a gagné
        fmt.Println("vous avez gagné !")
        data := Tours{
          tour{tableauMorpion,joueur1,1},
        }

        return data
    } else if partieNulle() { // Vérifier si match nul

        fmt.Println("Partie nulle !")
        data := Tours{
          tour{tableauMorpion,joueur1,2},
        }

        return data
    }
    joueur1 = !joueur1

    data := Tours{
      tour{tableauMorpion,joueur1,0},
    }

    return data

}

/**
* description de la fonction : Permet de remplir la case choisie par le joueur
*
* @param numeroCase
* @return rien
*/
func remplirCase(numeroCase int) {
    if joueur1 {
        tableauMorpion[numeroCase] = symboleJoueur1
    } else {
        tableauMorpion[numeroCase] = symboleJoueur2
    }
}

func test(channelIn chan int) {
    numeroCase := <-channelIn
    if joueur1 {
        tableauMorpion[numeroCase] = symboleJoueur1
    } else {
        tableauMorpion[numeroCase] = symboleJoueur2
    }
}

//
// func jouer() {
//
// }


/**
* description de la fonction : Permet de savoir si le joueur a gagné
*
* @return bool
*/
func gagner() bool {

    /*
        tableauxdeGain est un tableau à double dimensions où j'ai rajouté
        les différents cas d'utilisation où il est possible de gagner.
    */
    tableauxdeGain := [][tailleDamier]bool{
    {
        true, true, true,
        false, false, false,
        false, false, false},

    {
        false, false, true,
        false, false, true,
        false, false, true},
    {
        false, false, false,
        false, false, false,
        true, true, true},
    {
        true, false, false,
        true, false, false,
        true, false, false},
    {
        true, false, false,
        false, true, false,
        false, false, true},
    {
        false, false, true,
        false, true, false,
        true, false, false},
    {
        false, true, false,
        false, true, false,
        false, true, false}}

    // création d'un damier temporaire
    var tableauMorpionBool [tailleDamier]bool

    for index, valeur := range tableauMorpion {
        if joueur1 && valeur == symboleJoueur1 { // si c'est le tour du joueur 1 et que la case possède le bon symbole du joueur 1
            tableauMorpionBool[index] = true
        } else if !joueur1 && valeur == symboleJoueur2 {
            tableauMorpionBool[index] = true
        }
    }

    ressemblance := 0 // Nombre de true qui sont sur les mêmes cases dans le tableau tableauMorpionBool et dans le tableau tableauxdeGain
    for _, tableauGain := range tableauxdeGain {
        for i := 0; i < len(tableauMorpionBool); i++ {
            if tableauMorpionBool[i] == true && tableauMorpionBool[i] == tableauGain[i] { // si c'est à true dans le même index de tableauMorpionBool et tableauGain alors on incrémente la ressemblance
                ressemblance++
                if ressemblance == 3 { // si les cases du tableau tableauMorpionBool sont 3 fois les mêmes que sur l'un des tableaux tableauxdeGain alors ça veut dire que le joueur a gagné
                    return true
                }
            }
        }
        ressemblance = 0 // On remet le compteur à 0 pour vérifier un autre tableau du tableauxdeGain
    }
    return false
}

/*
* description de la fonction : Permet de vérifier si la partie est nulle
*
* @return bool
*/
func partieNulle() bool {
    occurence := 0

    for _, valeur := range tableauMorpion {
        if valeur == symboleJoueur1 || valeur == symboleJoueur2 {
            occurence++ // incrémenter de 1 si une case est remplite par un symbole
        }
    }

    /*
        si toutes les cases sont remplies de symboles
        et que le joueur n'a pas encore gagné alors la partie est nulle
    */
    return (occurence == len(tableauMorpion))
}


func foo() string {
    return "Foo"
}