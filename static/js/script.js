window.addEventListener("DOMContentLoaded", (event) => {

    let grille;

    let cases = document.getElementsByClassName("case");
    for(let i = 0; i < cases.length; i++) {

        cases[i].addEventListener("click", () => {

            if(!cases[i].classList.contains("full")){
                (async () => {
                    const rawResponse = await fetch('http://localhost:8003/jouer', {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            case: parseInt(cases[i].id),
                        })
                    });
                    const content = await rawResponse.json();

                    await remplirGrille(content);
                    if(content[0]["Victoire"] == 1){

                        victoire(content[0]["Joueur1"]);

                    } else if (content[0]["Victoire"] == 2) {
                        egalite()
                    }
                })();
            }
        });
    }


    let restartBtn = document.getElementsByClassName("restart");


    for (let i = 0; i < restartBtn.length; i++ ) {
        restartBtn[i].addEventListener("click", () => {
            document.location.reload(true);
        });
    }

});

function remplirGrille(grille) {

    (async () => {
        const rawResponse = await fetch('http://localhost:8003/', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });

        grille = await rawResponse.json();

        let damier = grille[0]["Damier"];

        for(let i = 0; i < damier.length; i++) {

            if(damier[i] == "X" )  {
                let laCase = document.getElementById(i.toString());
                if (!laCase.classList.contains("full")){
                    let img = document.createElement("img");
                    img.src = "/static/img/cross.png";

                    laCase.appendChild(img);
                    laCase.classList.add("full");
                }


            } else if (damier[i] == "O") {

                let laCase = document.getElementById(i.toString());

                if (!laCase.classList.contains("full")) {
                    let img = document.createElement("img");
                    img.src = "/static/img/circle.png";

                    laCase.appendChild(img);
                    laCase.classList.add("full");
                }
            }
        }

    })();
}

function victoire(joueur1) {

    let damier = document.getElementById("damier");
    damier.style.display = "none";

    let joueur;
    if (joueur1) {
        joueur = "JOUEUR 1";

    }else {
        joueur = "JOUEUR 2";
    }

    document.querySelector("#victoire h3").innerHTML += " VICTOIRE DU " + joueur  ;
    let victoire = document.getElementById("victoire");
    victoire.style.display = "flex";
}

function egalite() {
    let damier = document.getElementById("damier");
    damier.style.display = "none";

    document.querySelector("#egalite h3").innerHTML += " EGALITE ! PARTIE NULL! " ;
    let egalite = document.getElementById("egalite");
    egalite.style.display = "flex";
}

